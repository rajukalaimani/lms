package com.altr.lms;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import com.altr.lms.model.UserInfo;
import com.altr.lms.repository.ro.UserRepository;

@DataJpaTest
public class UserRepositoryTests {
	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	UserRepository userRepository;

	static Long userId=0L;
	
	@Test
	public void testFindByUsername() {
		UserInfo userInfoObj = new UserInfo("raju", "raju","raju.kalaimani81@gmail.com","+1 437 987 1982",  "admin", "A");
		entityManager.persist(userInfoObj);

		userInfoObj = new UserInfo("abi", "abi", "view","raju.kalaimani81@gmail.com","+1 437 987 1982",  "A");
		entityManager.persist(userInfoObj);

		userInfoObj = new UserInfo("tharun", "thaun", "raju.kalaimani81@gmail.com","+1 437 987 1982", "view", "A");
		entityManager.persist(userInfoObj);

		
		List<UserInfo> findByUsernameObj =null;
		try {
			findByUsernameObj = (List<UserInfo>)userRepository.findAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("TOTAL RECORD SIZE:>"+findByUsernameObj.size());
		for (UserInfo userInfo : findByUsernameObj) {
			System.out.println(userInfo.toString());
		}
		System.out.println("------------END RECORD ------------");
		assertThat(findByUsernameObj).extracting(UserInfo::getUserName).containsAnyOf(userInfoObj.getUserName());
	}
}