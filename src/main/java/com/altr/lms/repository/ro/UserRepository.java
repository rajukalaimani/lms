package com.altr.lms.repository.ro;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.altr.lms.model.UserInfo;

@Repository
public interface UserRepository extends CrudRepository<UserInfo,String> {

	public UserInfo findByUserName(String userName);
	
}
