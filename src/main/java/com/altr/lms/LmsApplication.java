package com.altr.lms;

import java.time.Instant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LmsApplication {

	public static void main(String[] args) {
		System.out.println("LMS Applicaion Started......"+Instant.now());
		SpringApplication.run(LmsApplication.class, args);
	}

}
