package com.altr.lms.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.altr.lms.exception.RecordNotFoundException;
import com.altr.lms.model.UserInfo;
import com.altr.lms.repository.ro.UserRepository;

@Component
public class UserService {

	@Autowired
	UserRepository userRepository;


	/** get user information for login validation **/
	public UserInfo getUserInfoByUserName(String userName, String password) throws Exception{
		System.out.println(String.format("UserService -----> getUserInfoByUserName ---> userName {%s}, password {%s}",userName,password));
		UserInfo userInfo = userRepository.findByUserName(userName);
		if(userInfo != null) {
			if(userInfo.getPassword() != null && userInfo.getPassword().equals(password)){
				return userInfo;
			}else {
				throw new Exception("User name or password is incorrect!!!");
			}
		}
		throw new RecordNotFoundException("User does not for the user name");
	}

	/** get user information for login validation **/
	public UserInfo getUserInfoByUserName(String userName) throws Exception{
		System.out.println(String.format("UserService -----> getUserInfoByUserName ---> userName {%s}",userName));
		UserInfo userInfo = userRepository.findByUserName(userName);
		if(userInfo != null) {
			return userInfo;
		} else {
			throw new RecordNotFoundException("User does not for the user name");
		}
	}
}
