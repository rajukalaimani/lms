package com.altr.lms.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.altr.lms.model.UserInfo;
import com.altr.lms.services.UserService;

@RestController
@RequestMapping("/lms")
public class LoginController {

	@Autowired
	UserService userService;

	@GetMapping("/authUser/{userName}/{password}")
	@ResponseBody
	/*public UserInfo authenticateUserCredential(@PathVariable Map<String,String> pathVarsMap) throws Exception{
		String userName = pathVarsMap.get("userName");
		String password = pathVarsMap.get("password");*/
	public UserInfo authenticateUserCredential(@PathVariable("userName") String userName,@PathVariable("password") String password) throws Exception{
		UserInfo userInfo=null;
		try {
			System.out.println(String.format("========authenticateUserCredential========> user name:{%s} =======> password:{%s}",userName,password));
			userInfo = userService.getUserInfoByUserName(userName,password);
		} catch (Exception e) {
			throw e;
		}
		return userInfo;
	}

	@GetMapping("/authUser/{userId}")
	@ResponseBody
	public UserInfo authenticateUser(@PathVariable("userId") String userName){
		UserInfo userInfo=null;
		try {
			System.out.println("========authenticateUser===========");
			userInfo = userService.getUserInfoByUserName(userName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return userInfo;
	}

}
