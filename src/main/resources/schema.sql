DROP TABLE IF EXISTS TBL_USER_PROFILE;
  
CREATE TABLE TBL_USER_PROFILE (
  USER_ID INT AUTO_INCREMENT  PRIMARY KEY,
  USER_NAME VARCHAR(250) NOT NULL,
  PASSWORD VARCHAR(250) NOT NULL,
  EMAIL_ID VARCHAR(250) NOT NULL,
  MOBILE_NUMBER VARCHAR(250) NOT NULL,
  USER_ROLE VARCHAR(250) DEFAULT 'READ_ONLY',
  USER_STATUS VARCHAR(1) DEFAULT 'A'  
);